import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class ExtendedAuthGuard extends AuthGuard('jwt') {
    handleRequest(err, user, info) {
        
        if (!user) {
            throw new UnauthorizedException();
        }

        return user;
    }
}