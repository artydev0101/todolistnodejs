import { ValidatorConstraintInterface, ValidatorConstraint, ValidationArguments } from "class-validator";

@ValidatorConstraint({ name: "isEqual", async: false })
export class IsEqualConstraint implements ValidatorConstraintInterface {

    validate(propertyValue: string, args: ValidationArguments) {
        return propertyValue === args.object[args.constraints[0]];
    }

    defaultMessage(args: ValidationArguments) {
        return `"${args.property}" must be equal to "${args.constraints[0]}"`;
    }
}