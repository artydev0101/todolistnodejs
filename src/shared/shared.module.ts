import { Module } from '@nestjs/common';
import { SchemaModule } from '../schema/schema.module';
import { UserModule } from '../user/user.module';

@Module({
    imports: [SchemaModule, UserModule],
    exports: [SchemaModule, UserModule],
})
export class SharedModule { }
