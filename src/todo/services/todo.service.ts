import { Injectable } from '@nestjs/common';
import { ResponseGetAllTodosModel, ResponseTodoGetAllTodosModelItem } from '../models/response-get-all-todos-todo.model';
import { RequestCreateTodoModel } from '../models/request-create-todo-todo.model';
import { ResponseGetTodoModel } from '../models/response-get-todo-todo.model';
import { ITodoService } from './interfaces/todo.service.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../../schema/documents/user.document';
import { Todo } from '../../schema/documents/todo.document';
import { RequestUpdateTodoModel } from '../models/request-update-todo-todo.model';

@Injectable()
export class TodoService implements ITodoService {

    constructor(
        @InjectModel('User') private readonly userModel: Model<User>,
        @InjectModel('Todo') private readonly todoModel: Model<Todo>,
    ) { }

    public async create(createTodoModel: RequestCreateTodoModel, userEmail: string): Promise<void> {
        const user = await this.userModel.findOne({ email: userEmail }).exec();

        console.log(createTodoModel);
    
        const createdTodo = new this.todoModel(createTodoModel);   
        
        console.log(createdTodo);
        

        user.todoItems.push(createdTodo);

        await user.save();
    }

    public async update(updateTodoModel: RequestUpdateTodoModel, userEmail: string): Promise<void> {

        const user = await this.userModel.findOne({ email: userEmail }).exec();

        const oldItem = user.todoItems.find(ti => ti.id === updateTodoModel.id);

        const updatedItem = JSON.parse(JSON.stringify(oldItem)) as Todo;

        updatedItem.description = updateTodoModel.description;
        updatedItem.isDone = updateTodoModel.isDone;
        updatedItem.image = updateTodoModel.image;

        const index = user.todoItems.indexOf(oldItem);

        user.todoItems[index] = updatedItem;

        await user.save();
    }

    public async getAll(userEmail: string): Promise<ResponseGetAllTodosModel> {
        const user = await this.userModel.findOne({ email: userEmail }).exec();        

        if (!user.todoItems) {
            const response: ResponseGetAllTodosModel = {
                todoList: []
            };
            return response;
        }

        const response: ResponseGetAllTodosModel = {
            todoList: user.todoItems.map((value) => {                
                const response: ResponseTodoGetAllTodosModelItem = {
                    id: value.id,
                    isDone: value.isDone,
                    title: value.title,
                    image: value.image
                };
                return response;
            })
        }

        return response;
    }

    public async getById(id: string, userEmail: string): Promise<ResponseGetTodoModel> {

        const todo = await this.getTodoById(id, userEmail);

        const result: ResponseGetTodoModel = {
            isDone: todo.isDone,
            id: todo.id,
            description: todo.description,
            title: todo.title,
            image: todo.image,
            location: todo.location
        }

        return result;
    }

    public async delete(id: string, userEmail: string): Promise<void> {
        const user = await this.userModel.findOne({ email: userEmail }).exec();

        user.todoItems = user.todoItems.filter((item) => {
            return item.id != id;
        });

        await user.save();
    }

    public async getTodoById(id: string, userEmail: string): Promise<Todo> {
        const user = await this.userModel.findOne({ email: userEmail }).exec();

        const todo = user.todoItems.find(ti => ti.id === id);

        return todo;
    }
}
