import { ResponseGetTodoModel } from "../../../todo/models/response-get-todo-todo.model";
import { ResponseGetAllTodosModel } from "../../../todo/models/response-get-all-todos-todo.model";
import { RequestCreateTodoModel } from "../../../todo/models/request-create-todo-todo.model";

export interface ITodoService {
    create(createTodoModel: RequestCreateTodoModel, userEmail: string): Promise<void>;

    getAll(userEmail: string): Promise<ResponseGetAllTodosModel>;

    getById(id: string, userEmail: string): Promise<ResponseGetTodoModel>;

    delete(id: string, userEmail: string): Promise<void>;
}