import { Controller, Get, Param, Post, Body, UseGuards, HttpCode } from '@nestjs/common';
import { ResponseGetTodoModel } from '../models/response-get-todo-todo.model';
import { ResponseGetAllTodosModel } from '../models/response-get-all-todos-todo.model';
import { TodoService } from '../services/todo.service';
import { RequestCreateTodoModel } from '../models/request-create-todo-todo.model';
import { AuthGuard } from '@nestjs/passport';
import { RequestUpdateTodoModel } from '../models/request-update-todo-todo.model';
import { ExtendedAuthGuard } from '../../shared/guards/extended-auth.guard';
import { UserDecorator } from '../../shared/decorators/user.decorator';
import { User } from 'src/schema/documents/user.document';

@Controller('todo')
export class TodoController {

    constructor(private readonly todoService: TodoService) {
    }

    @UseGuards(ExtendedAuthGuard)
    @HttpCode(200)
    @Get('/getAll')
    async getAll(@UserDecorator() user): Promise<ResponseGetAllTodosModel> {
        var todoList = await this.todoService.getAll((user as User).email);
        return todoList;
    }

    @UseGuards(ExtendedAuthGuard)
    @Post('/create')
    async create(@UserDecorator() user, @Body() requestCreateTodoModel: RequestCreateTodoModel): Promise<void> {        
        await this.todoService.create(requestCreateTodoModel, (user as User).email);
    }

    @UseGuards(ExtendedAuthGuard)
    @Post('/update')
    async update(@UserDecorator() user, @Body() requestUpdateTodoModel: RequestUpdateTodoModel): Promise<void> {
        await this.todoService.update(requestUpdateTodoModel, (user as User).email);
    }

    @UseGuards(ExtendedAuthGuard)
    @Get('/getById/:id')
    async getById(@UserDecorator() user, @Param('id') id: string): Promise<ResponseGetTodoModel> {
        var todo = await this.todoService.getById(id, (user as User).email);
        return todo;
    }

    @UseGuards(ExtendedAuthGuard)
    @Get('/delete/:id')
    async delete(@UserDecorator() user, @Param('id') id: string): Promise<void> {
        await this.todoService.delete(id, (user as User).email);
    }
}