import { Location } from "../../shared/models/location.model";

export class ResponseGetTodoModel{
    id: string;
    title: string;
    description: string;
    isDone: boolean;
    location: Location;
    image: string;
}