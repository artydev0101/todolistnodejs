import { IsMongoId, IsNotEmpty, IsOptional, IsBoolean, IsBase64, IsBooleanString } from "class-validator";

export class RequestUpdateTodoModel {
    @IsNotEmpty()
    @IsMongoId()
    id: string;

    @IsNotEmpty()
    description: string;

    @IsBoolean()
    isDone: boolean;

    @IsBase64()
    @IsOptional()
    image: string;
}