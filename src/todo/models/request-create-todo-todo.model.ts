import { Location } from "../../shared/models/location.model";
import { IsNotEmpty, IsBoolean, IsBase64, IsOptional, IsBooleanString } from "class-validator";

export class RequestCreateTodoModel {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    description: string;

    @IsBoolean()
    isDone: boolean;

    @IsOptional()
    location: Location;

    @IsBase64()
    @IsOptional()
    image: string;
}