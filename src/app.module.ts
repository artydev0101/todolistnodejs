import { Module } from '@nestjs/common';
import { TodoModule } from './todo/todo.module';
import { IdentityModule } from './identity/identity.module';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from './shared/shared.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/todo', { useNewUrlParser: true }), 
    TodoModule, 
    IdentityModule, 
    SharedModule
  ],
})
export class AppModule { }
