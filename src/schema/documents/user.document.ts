import { Document } from 'mongoose';
import { Todo } from '../documents/todo.document';

export interface User extends Document {
    id: string,
    email: string,
    password: string,
    todoItems: Array<Todo>
}