import { Document } from 'mongoose';
import { Location } from '../../shared/models/location.model';

export interface Todo extends Document {
    id: string
    title: string
    description: string
    isDone: boolean
    location: Location
    image: string
}