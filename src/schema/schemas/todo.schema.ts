import * as mongoose from 'mongoose';

export const TodoSchema = new mongoose.Schema({
  title: String,
  description: String,
  isDone: Boolean,
  location: Object,
  image: String,
});