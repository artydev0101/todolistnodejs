import * as mongoose from 'mongoose';
import { TodoSchema } from './todo.schema';

export const UserSchema = new mongoose.Schema({
  email: String,
  password: String,
  todoItems: [TodoSchema],
});