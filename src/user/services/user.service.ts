import { Injectable, BadRequestException } from '@nestjs/common';
import { IUserService } from './interfaces/user.service.interface';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RequestSignUpIdentityModel } from '../../identity/models/request-sign-up-identity.model';
import { User } from '../../schema/documents/user.document';

@Injectable()
export class UserService implements IUserService {

    constructor(
        @InjectModel('User') private readonly userModel: Model<User>
    ) {
    }

    async findByEmail(email: string): Promise<User> {
        const user = await this.userModel.findOne({ email: email }).exec();
        return user;
    }

    async create(requestSignUpIdentityModel: RequestSignUpIdentityModel): Promise<User> {
        const user = await this.findByEmail(requestSignUpIdentityModel.email);

        if (user) {
            throw new BadRequestException('User already exists');
        }

        const createdUser = new this.userModel(requestSignUpIdentityModel);

        const result = await createdUser.save();

        return result;
    }
}
