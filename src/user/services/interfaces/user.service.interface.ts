import { User } from "../../../schema/documents/user.document";

export interface IUserService{
    findByEmail(email: string): Promise<User>
}