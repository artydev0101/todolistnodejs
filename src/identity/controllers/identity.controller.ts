import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { UserService } from '../../user/services/user.service';
import { RequestSignUpIdentityModel } from '../models/request-sign-up-identity.model';
import { RequestSignInIdentityModel, RequestSocialSignInIdentityModel } from '../models/request-sign-in-identity.model';
import { ResponseSignInIdentityModel } from '../models/response-sign-in-identity.model';

@Controller('identity')
export class IdentityController {

    constructor(
        private readonly authService: AuthService,
        private readonly userService: UserService,
    ) { }

    @Post('/sign-in')
    async signIn(@Body() requestSignInIdentityModel: RequestSignInIdentityModel): Promise<ResponseSignInIdentityModel> {
        const responseSignInIdentityModel = await this.authService.signIn(requestSignInIdentityModel);
        return responseSignInIdentityModel;
    }

    @Post('/social-sign-in')
    async socialSignIn(@Body() requestSocialSignInIdentityModel: RequestSocialSignInIdentityModel): Promise<ResponseSignInIdentityModel> {
        const responseSignInIdentityModel = await this.authService.socialSignIn(requestSocialSignInIdentityModel);
        return responseSignInIdentityModel;
    }

    @Post('/sign-up')
    async signUp(@Body() requestSignUpIdentityModel: RequestSignUpIdentityModel): Promise<void> {
        await this.userService.create(requestSignUpIdentityModel);
    }
}
