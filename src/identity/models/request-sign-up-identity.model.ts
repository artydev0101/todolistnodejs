import { IsNotEmpty, MinLength, Matches, Validate } from 'class-validator';
import { IsEqualConstraint as IsEqual } from '../../shared/validators/is-equal.constraint';
import { RegExpConstants } from '../../shared/constants/regexp-constants';

export class RequestSignUpIdentityModel {
    @Matches(RegExpConstants.EmailRegExp)
    @IsNotEmpty()
    email: string;

    @MinLength(6)
    password: string;

    @Validate(IsEqual, ['password'])
    passwordConfirmation: string;
}