import { IsNotEmpty, MinLength, Matches } from 'class-validator';
import { RegExpConstants } from '../../shared/constants/regexp-constants';

export class RequestSignInIdentityModel {
    @Matches(RegExpConstants.EmailRegExp)
    @IsNotEmpty()
    email: string;

    @MinLength(6)
    password: string;
}

export class RequestSocialSignInIdentityModel {
    @IsNotEmpty()
    email: string;
}