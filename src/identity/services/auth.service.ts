import { JwtService } from '@nestjs/jwt';
import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtPayload } from '../models/jwt-payload.interface';
import { UserService } from '../../user/services/user.service';
import { User } from '../../schema/documents/user.document';
import { RequestSignInIdentityModel, RequestSocialSignInIdentityModel } from '../models/request-sign-in-identity.model';
import { ResponseSignInIdentityModel } from '../models/response-sign-in-identity.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) { }

  async signIn(requestSignInIdentityModel: RequestSignInIdentityModel): Promise<ResponseSignInIdentityModel> {

    const user = await this.userService.findByEmail(requestSignInIdentityModel.email);

    if (!user || user.password !== requestSignInIdentityModel.password) {
      throw new BadRequestException('Wrong email or password');
    }

    return this.getSignInResult(user.email);
  }

  async socialSignIn(requestSocialSignInIdentityModel: RequestSocialSignInIdentityModel): Promise<ResponseSignInIdentityModel> {

    var user = await this.userService.findByEmail(requestSocialSignInIdentityModel.email);

    if (!user) {
      const password = Math.random().toString(36).slice(2);
      user = await this.userService.create({
        email: requestSocialSignInIdentityModel.email,
        password: password,
        passwordConfirmation: password
      });
    }

    return this.getSignInResult(user.email);
  }

  private getSignInResult(email: string): ResponseSignInIdentityModel {
    const jwtPayload: JwtPayload = { email: email };

    const response: ResponseSignInIdentityModel = {
      accessToken: this.jwtService.sign(jwtPayload)
    }

    return response;
  }

  async validateUser(payload: JwtPayload): Promise<User> {
    return await this.userService.findByEmail(payload.email);
  }
}