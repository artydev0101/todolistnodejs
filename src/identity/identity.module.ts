import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './services/auth.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { IdentityController } from './controllers/identity.controller';
import { SharedModule } from '../shared/shared.module';

@Module({
    imports: [
        PassportModule.register(
            { 
                defaultStrategy: 'jwt',
                property: 'user',
                session: true
            }),
        JwtModule.register({
            secret: 'secretKey',
            signOptions: {
                expiresIn: 100000,
            },
        }),
        SharedModule
    ],
    providers: [AuthService, JwtStrategy],
    exports: [PassportModule, AuthService],
    controllers: [IdentityController],
})
export class IdentityModule { }